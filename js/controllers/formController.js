var form = angular.module('FormCtrl',[]);
 
form.controller('FormController',function($scope,$location,$http){

//Sending Address to Google API 
	$scope.sendData = function(address) {
		sendAddress = address.street + '' + address.houseNumber+ ', '+ address.postalCode + ', '+address.city ;
  		var params = {address: sendAddress, sensor: false};
	    //Note: google map rejects XHR header
	    $http.get('http://maps.googleapis.com/maps/api/geocode/json', {params: params, headers: {'X-Requested-With': undefined}})
	        .success(function(data, status, headers, config) {
	        	console.log(data) ;

	        	if(data.status=='ZERO_RESULTS'){
	        		$.notify({
		                message: 'The Address is invalid'
		            },{
		                type: 'danger',
		                z_index:9999,
		                delay:2000,
		                animate: {
		                    enter: 'animated bounceIn',
		                    exit: 'animated bounceOut'
		                }
		       	});

		       	address.successful = false; 
	        	}

	        	else{
		     		address.location = data.results[0].geometry.location ; 
		        	$.notify({
			                message: 'The Address is found with coordinates {{'+address.location.lat+'}}, {{'+address.location.lng+'}}'    
			            },{
			                type: 'success',
			                z_index:9999,
			                delay:2000,
			                animate: {
			                    enter: 'animated bounceIn',
			                    exit: 'animated bounceOut'
			                }
			       	});
		       	address.successful = true; 
				}
	        })
	        .error(function(data, status, headers, config) {
	        	address.successful = false; 

	        })

	    return address ;
	}
});




