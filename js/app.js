
var app = angular.module('formApp',[
'ngRoute',
'ngResource',
//Form Controller
'FormCtrl',
//UI Bootstrap
'ui.bootstrap'
]);

app.run(function(){
 
});


app.config(['$httpProvider', function($httpProvider) {
//enable XMLHttpRequest, to indicate it's ajax request
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}])

//This will handle all of the routing
app.config(function($routeProvider, $locationProvider){
 
$routeProvider.when('/',{
templateUrl:'form.html',
controller:'FormController'
});

 
});




